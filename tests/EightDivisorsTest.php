<?php 

require_once __DIR__.'/bootstrap.php';
/**
 * Description of EightDivisorsTest
 *
 * @author hip
 */
class EightDivisorsTest extends PHPUnit_Framework_TestCase {
    
    public function setUp()
    {
        parent::setUp();
        $this->eightdivisors = new EightDivisors();
    }
    
    public function tearDown() 
    {
        parent::tearDown();
        unset($this->eightdivisors);
    }        

    public function testCanStoreMaxLimit()
    {
        $maxLimit = 100;
        $this->eightdivisors->setMaxLimit($maxLimit);
        $this->assertEquals($maxLimit, $this->eightdivisors->getMaxLimit());
    }
    
    public function testHasMaxLimitByDefault()
    {        
        $max = $this->eightdivisors->getMaxLimit();
        $this->assertEquals(100,$max);
    }
    
    public function testReturnsArrayOnCallForDivisorsFor()
    {
        $for = 30;
        $divisors = $this->eightdivisors->divisorsFor($for);
        $this->assertInternalType('array', $divisors);
        $this->assertEquals(8,count($divisors));
        
        $for = 24;
        $divisors = $this->eightdivisors->divisorsFor($for);
        $this->assertEquals(8,count($divisors));
    }
    
    public function testReturnsItSelfAndOneInCasePrime()
    {
        $for = 5;
        $divisors = $this->eightdivisors->divisorsFor($for);        
        $this->assertTrue(in_array($for,$divisors));
        $this->assertTrue(in_array(1,$divisors));
    }
    
    public function testReturnCountOneLessThanMaxLimit()
    {
        $maxLimit = 100;
        $this->eightdivisors->setMaxLimit(100);
        $upToMaxLimit = $this->eightdivisors->getDivisorsUpToMaxLimit();
        $this->assertCount($maxLimit-1, $upToMaxLimit);        
    }
    
    public function testCanReturnCountOnlyAsInt()
    {
        $maxLimit = 100;
        $this->eightdivisors->setMaxLimit(100);
        $upToMaxLimit = $this->eightdivisors->getDivisorsUpToMaxLimit(true);
        $this->assertCount($maxLimit-1, $upToMaxLimit);
        $rand = rand(2, count($upToMaxLimit));
        $this->assertInternalType('integer', $upToMaxLimit[$rand]);
    }
    
    public function testReturnsMatchingCountOnly()
    {
        $matchCount = 8;
        $maxLimit = 100;
        $this->eightdivisors->setMaxLimit($maxLimit);
        $matching = $this->eightdivisors->getMatchingDivisorCountFor($matchCount);
        $this->assertContains(24, $matching);
        $this->assertCount(10, $matching);
        
        $maxLimit = 1000;
        $this->eightdivisors->setMaxLimit($maxLimit);
        $matching = $this->eightdivisors->getMatchingDivisorCountFor($matchCount);
        $this->assertContains(24, $matching);
        $this->assertCount(180, $matching);                
        
//        $maxLimit = pow(10,6);        
//        $this->eightdivisors->setMaxLimit($maxLimit);
//        $matching = $this->eightdivisors->getMatchingDivisorCountFor($matchCount);
//        $this->assertContains(24, $matching);
//        $this->assertCount(224427, $matching);
    }
}
