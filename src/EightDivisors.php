<?php  

/**
 * Description of EightDivisors
 *
 * @author hip
 */
class EightDivisors {
    
    protected $maxLimit;
    
    public function __construct($maxLimit = null) {        
        if(is_null($maxLimit)){
            $maxLimit = 100;
        }
        $this->maxLimit = $maxLimit;
    }

    public function setMaxLimit($maxLimit)
    {
        $this->maxLimit = $maxLimit;
    }
    
    public function getMaxLimit()
    {
        return $this->maxLimit;
    }
    
    public function divisorsFor($num)
    {        
        $divisors = [];
        for($i=1; $i <= ceil(sqrt($num)); $i++)
        {
            if($num % $i == 0)
            {
                
                if(!in_array($i, $divisors))
                {
                    $divisors[] = $i;
                }
                if(!in_array($num/$i, $divisors))
                {
                    $divisors[] = $num/$i;
                }
                
            }
        }
        
        return $divisors;
    }
    
    public function getDivisorsUpToMaxLimit($countOnly = false)
    {
        $upToMaxLimit = [];
        for($i=1; $i < $this->maxLimit; $i++ )
        {
            if($countOnly == false)
            {
                $upToMaxLimit[$i] = $this->divisorsFor($i);
            }else{
                $upToMaxLimit[$i] = count($this->divisorsFor($i));
            }
            
        }
        
        return $upToMaxLimit;
    }
    
    public function getMatchingDivisorCountFor($matchCount)
    {        
        $divisorCount = $this->getDivisorsUpToMaxLimit(true);
        $filtered = array_filter($divisorCount, function($v) use ($matchCount)
                {
                    return $v == $matchCount;
                });
        
        return array_keys($filtered);
    }
    
}
