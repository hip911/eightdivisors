<?php

/**
 * Description of EightDivisorsMath
 *
 * @author hip
 */
class EightDivisorsMath {
    
    protected $limit;
    protected $primeLimit;
    protected $primes = [];


    public function __construct($limit) {
        $this->limit = $limit;
        $this->primeLimit = ceil(pow($this->limit/2,1/3));
    }
    
    public function calc()
    {
        return $this->calcFirstTypeCount()+$this->calcSecondTypeCount()+$this->calcThirdTypeCount();
    }
    
    public static function getPrimes($limit)
    {
        $count = 0;
        $x=2;
        //$limit = ceil(pow($limit,1/3));
        //$primes = [];
        while($x<=$limit)
        {
            $count += 1;
            //$primes[] = (int) $x;
            $x = (int) static::nPr($x);            
        }
        //return count($primes);
        return $count;
    }
    
    function getPrimesSlow($limit)
    {
        $limit = ceil(pow($limit,1/3));
        $number = 2;
        $range = range($number,$limit);
        $primes = array_combine($range,$range);
        while($number*$number < $limit){
            for($i=$number; $i<=$limit; $i+=$number){
                if($i==$number){
                    continue;
                }
                unset($primes[$i]);
            }
            $number = next($primes);
        }
        return count($primes);
    }
    
    function fillPrimesSlow()
    {
        $newlimit = ceil(pow($this->limit,1/2));
        for($x=2;$x <= $newlimit; $x=$this->nPr($x) )
        {
            $this->primes[] = (int) $x;
        }
    }
    
    function fillPrimes()
    {
        $newlimit = ceil(pow($this->limit,1/1.2));
        for($x=2;$x <= $newlimit; $x=$this->nPr($x) )
        {
            $this->primes[] = (int) $x;
        }
        return count($this->primes);
    }

    //(x*x*x*y)
    function calcFirstTypeCount(){
        $count = 0;
        $end = false;
        $lastfilled = $this->primes[count($this->primes)-1];
        foreach( $this->primes as $x )
        {   
            $xcube = pow($x,3);
            $uplimit = floor($this->limit/2);
            if($xmax > $uplimit)
            {
                break;
            }
            foreach ($this->primes as $y )
            {                
                if($x==$y){
                    continue;
                }
                $xcubey = pow($x,3)*$y;                
                if($xcubey < $this->limit)
                {
                    $count += 1;
                }else{
                    break;
                }
            }
            $uplimit = $this->limit/$xcube;
            for($y=$lastfilled; $y <= $uplimit; $y=$this->nPr($y) )
            {                
                if($x==$y){
                    continue;
                }
                if((pow($x,3))*$y < $this->limit)
                {
                    $count += 1;
                }
            }
            
        }
        return $count;
    }   
//    //(x*x*x*y)
//    function calcFirstTypeCount(){
//        $count = 0;
//        for( $x=2; $x <= ceil(pow($this->limit/2,1/3)); $x=$this->nPr($x) )
//        {
//            for($y=2; $y <= ($this->limit/pow($x,3)); $y=$this->nPr($y) )
//            {                
//                if($x==$y){
//                    continue;
//                }
//                if((pow($x,3))*$y < $this->limit)
//                {
//                    $count += 1;
//                }
//            }                        
//        }
//        return $count;
//    }
       
    
    //x*y*z
    function calcSecondTypeCount(){
        $count = 0;
        for($x=2; $x<=ceil(pow($this->limit,1/3)); $x=$this->nPr($x) )
        {
            for($y=$this->nPr($x); $y<=ceil(pow($this->limit/$x,1/2)); $y=$this->nPr($y) )
            {
                for($z=$this->nPr($y); $z<=ceil($this->limit/($x*$y)); $z=$this->nPr($z) )
                {
                    if($x*$y*$z < $this->limit)
                    {
                        $count += 1;
                    }
                }
            }
        }
        return $count;
    }
    
    //x^7
    function calcThirdTypeCount(){
        $count = 0;
        for( $x=2; $x <= ceil(pow($this->limit,1/7)); $x=$this->nPr($x) )
        {
            if(pow($x,7) < $this->limit)
            {
                $count += 1;
            }
        }
        return $count;
    }
    
    public function testnPrSpeed() {
        for($i=1;$i<1000000;$i++)
        {
            $this->nPr(1225252);
        }
        
    }    
    
    //nextprime helper
    private static function nPr($int)
    {
        return gmp_strval(gmp_nextprime($int));
    }
    
    
}
//cacheing
/*$time_start = microtime(true);
$EDM = new EightDivisorsMath(pow(10,10));
$count = $EDM->fillPrimes();
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Cached $count items in $time seconds\n";*/

//calc
/*$time_start = microtime(true);
$result = $EDM->calcFirstTypeCount();
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "Answer of $result done in $time seconds\n";*/

$limit = pow(10,8);
$time_start = microtime(true);
$count = EightDivisorsMath::getPrimes($limit);
$time_end = microtime(true);
$time = $time_end - $time_start;
echo "$count primes under $limit in $time seconds\n";


?>
