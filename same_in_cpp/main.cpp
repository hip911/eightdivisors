/* 
 * File:   main.cpp
 * Author: hip
 *
 * Created on 28 March 2015, 09:57
 */

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <gmp.h>
#include <math.h> 
#include <sys/time.h>

using namespace std;

#define BILLION 1E9
/*
 * 
 */
int main(int argc, char** argv) {
    //timespec ts;
    struct timespec requestStart, requestEnd;
    clock_gettime(CLOCK_REALTIME, &requestStart);
    int x = 2;
    int num;
    int count = 0;
    int limit = 100000000;
    mpz_t n,m;
    //mpz_init_set_str(n,"999",10);
    mpz_init_set_str(n,"2",10);
    mpz_init_set_str(m,"2",10);
    
    for(mpz_set_si(n,2);mpz_get_si(n)<=ceil(pow(limit/2,0.333));mpz_nextprime(n,n)){
        for(mpz_set_si(m,2);mpz_get_si(m)<limit/pow(mpz_get_si(n),3);mpz_nextprime(m,m)){
            if(mpz_get_si(n) == mpz_get_si(m))
            {
                continue;
            }
            num = mpz_get_si(n)*mpz_get_si(n)*mpz_get_si(n)*mpz_get_si(m);
            //cout << num <<endl;
            if(num < limit)
            {
                //cout << num <<endl;
                //gmp_printf("%Zd\n",n);
                count += 1;
            }
            
            //mpz_clear(m);
        }
        //mpz_clear(n);
    }
        //mpz_nextprime(n,n);
    //count += 1;
    //gmp_printf("%Zd\n",n);
    clock_gettime(CLOCK_REALTIME, &requestEnd);
    cout << count <<endl;
    //printf( "%i\n", clock_gettime(CLOCK_REALTIME, &ts) );
    double accum = ( requestEnd.tv_sec - requestStart.tv_sec )
  + ( requestEnd.tv_nsec - requestStart.tv_nsec )
  / BILLION;
printf( "%lf\n", accum );
    return 0;
}

